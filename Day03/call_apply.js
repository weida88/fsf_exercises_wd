/**
 * Created by weida88 on 12/10/2016.
 */
// var obj = {
//     name: "",
//     say: function(prefix, value2){
//         console.log(prefix);
//         console.log(this.name);
//         console.log(prefix + ' ' + this.name);
//         console.log(value2 + ' ' + this.name);
//
//         return prefix + value2 + this.name;
//     }
// };
//
// var newObject = {name:"Alex"};
// var result = obj.say.call(newObject, "Hello", "How are you");
// console.log(result);

//example 2

var obj2 = {
    name: "",
    age: 0,
    email: "",
    siblings: [],
    say : function(greeting1, greeting2){
        console.log(greeting1);
        console.log(greeting2);
        console.log(this.siblings);

        console.log(this.name);
        return greeting1 + ' ' + this.name + ' ' + this.age;
    }
};

var newObject2 = { name: "Sam", age: 45, siblings: ['Mary', 'Jane']};
var result4= obj2.say.apply(newObject2, ['Hello', 'there']);
console.log(result4)