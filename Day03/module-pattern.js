/**
 * Created by weida88 on 12/10/2016.
 */
//module-pattern.js
var a = require('./a.js'); // importing a.js
var b = require('./b.js');

// console.log(a);
// console.log(a.bWasloaded);
// console.log(a.age);
console.log(a.loaded2);

// console.log(a.loaded3);

console.log(a.drive());
console.log(b.run());
//console.log(a.bWasloaded);

// console.log(b);
// console.log(b.aWasloaded);