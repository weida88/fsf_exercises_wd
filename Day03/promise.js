/**
 * Created by weida88 on 12/10/2016.
 */
// "resolve" match with "then"
// "reject" match with "catch"
var myPromise = new Promise(function(resolve, reject){
    setTimeout(function(){
        resolve("some value");
    },2000);
});

myPromise.then(function(){
    console.log("Accepted");
}).catch(function(){
    console.log("Rejected");
});

//Separate example
function isValid(colour){
    return new Promise(function(resolve2,reject){
        if(colour=='BLUE'){
            //Resolve("some value")
            resolve2("valid colour");
        } else if (colour == 'RED'){
            resolve2('valid colour');
        } else{
            reject("invalid colour");
        }
    });
}

isValid("Yellow").then(function(result){
    console.log(result);
}).catch(function(err){
    console.log(err);
});