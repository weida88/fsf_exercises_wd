/**
 * Created by weida88 on 12/10/2016.
 */
//a.js
var b = require('./b'); //import b.js in

exports.loaded = true; //public. "exports" is the key word that makes the "loaded" attribute public
exports.loaded3 = 44;

var x = 9; // local

//Public
module.exports = {      // module.exports is to make b public
    bWasloaded:b.loadedInstance,
    loaded2: true,
    age: 18,
    drive: function(){
        console.log("I am driving");
        return "I am driving";
    }
};