/**
 * Created by weida88 on 12/10/2016.
 */
var N = 0;

function isEven(N){
    return new Promise(function(resolve,reject){
        if(typeof N === "number"){
            if(N == 0){
                resolve("Its ZERO!");
            }   else if(N%2 == 0) {
                resolve("Its EVEN!");
            } else{
                resolve("Its ODD!");
            }
        }
        else{
            reject("INVALID Number!");
        }
    });
}

isEven(2).then(function(result){
    console.log(result);
}).catch(function(err){
    console.log(err);
});