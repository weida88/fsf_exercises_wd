/**
 * Created by weida88 on 12/10/2016.
 */
//b.js
exports.loadedInstance = false;

var a = require('./a'); //import a.js in

module.exports = {      // module.exports is to make b public
    aWasloaded:a.loaded,
    loaded : true,
    run: function(){
        console.log("Running");
        return "Running"; // for function must have a return value, if not console.log will print 'undefined'
    }
};