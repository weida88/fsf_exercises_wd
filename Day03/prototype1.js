/**
 * Created by weida88 on 12/10/2016.
 */
// prototype1.js
var TeslaModelS = function(){
    this.numWheels = 4;
    seats = 5;
    this.manufacturer = "tesla";
    this.make = "ModelS";
    var belts = 5;
}

TeslaModelS.prototype = function(){
    var go = function(){
      console.log("rotate wheels");
    };

    var stop = function(){
      console.log("stop wheels");
    };

    return {
        pressBrakePedal: stop,
        jamBrake: stop,
        pressGasPedal: go
    }
}();

module.exports = TeslaModelS;