function a(N,successCallback,errorCallback){
    if (N > 18){
        successCallback();
    }
    else{
        errorCallback();
    }
}

// var successCallback = function () {
//     console.log("success");
// }
//
// var errorCallback = function(){
//     console.log("error");
// }

a(
    19,
    function () {
        console.log("success");
    },
    function(){
        console.log("error");
    }
);

a(
    18,
    function () {
        console.log("success");
    },
    function(){
        console.log("error");
    }
);

// a(19,successCallback,errorCallback);
// a(17,successCallback,errorCallback);