var N = 5;
var sum = 0;
var a = [];

var element = 4;
var index;

for (var i=0; i<=N; i++){
    a.push(i);
}

//write code to find out index of element
//way 1 - one line
index = a.indexOf(element);

//way 2
a.forEach(function (item, i){
    if(element == item){
        index = i;
    }
})

console.log(index);
